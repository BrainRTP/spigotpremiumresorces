## Command:
`/hm reload` - Reload config.yml _(premission: hm.admin)_

---

## Config.yml
```YAML
# Permission to enable holo message. Set "" to disable holo message.
permission: ""

# Holograms settings
holograms:
  maxLines: 5
  lifeTime: 5
  animation: true
  showToSender: false
  # Default: 0
  # Use it, if you need correct hologram location.
  # Example: 2.3
  offSetY: 0

blackList:
  # Set regions where holo messages will not be displayed.
  # Use [] to disable. Example:
  # regions : []
  regions:
    - spawn
    - pvp
  # Set worlds where holo messages will not be displayed.
  # Use [] to disable. Example:
  # worlds : []
  worlds:
    - someWorld
```