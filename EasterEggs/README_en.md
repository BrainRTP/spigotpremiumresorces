# EasterEggs

This plugin allows you to create easter eggs (blocks, entities, NPC). By clicking  
them u can run various actions and events.

## Installation

Copy the .jar file to your plugins folder. Run the server. The error will notice that  
database 'Paste me' is not supported. It's ok. Now, go to the config.conf.
You will see the following:

```json
database{
    type: "Paste me" // Supported DMS: MySQL, SQLite
    host: "~"
    database: "db"
    port: 3306
    user: "root"
    password: "root"
}

glowTime: 5 //Easter eggs glow time in seconds
```

Change the DMS to the one you need. For local databases host will be the path
to the folder with the *.db file. For the local database you should specify a file name in the database line.
For MySQL by default.

To get help about the plugin type `/ee help`.

## Groups
To create a new group type `/ee create <your_name>`.

To delete any group type `/ee delete <name>`. If easter eggs from this group were already found by someone
they will be deleted from the database.

## Creating easter eggs
Once you've created the group u can start editing it and adding new easter eggs by 
typing `/ee edit <your_group_name>`.

To add a new easter egg just click RMB on any block/armorstand/painting/frame/npc.
A new easter eggs with unique ID will be created. 

To remove any easter egg just click LBM on it while editing.

## Adding actions to easter eggs
Easter eggs actions are adding in edit mode. To add an action just type 
`/ee action add <easter egg id> <action name> <action data>`

`<action name>` value is the name of one of the following action types:

`message` - Chat/aactiobar/title message.  
`command` - Running commands by the player or server
`sound` - Playing sound for the player
`money` - Gives any amount of money  
`firework` - Launching a firework  

Those actions are in the json format. Data depends on the variables of 
specified action. E.g. `message` action can take the following variables:

`messages` - List of messages displaying in the chat. That's how it looks in  
json - {messages:["Message 1", "Message 2"]}  
`title` - Title message  
`subtitle`- Subtitle message  
`fadeIn` - Title fade in period in ticks (1 second = 20 ticks)  
`fadeOut` - Title fade out period in ticks
`stay` - Delay period before fade out starts in ticks
`actionbar` - Actionbar message

For example, if u want to display something in actionbar, you should type:
`/ee action add <id> message {actionbar:"Hello, world!"}`

Some variables can't live without each other, for example 
`title` and `subtitle` require `fadeIn`, `fadeOut` and `stay`.

Here is the list of all actions' variables.

#### command
`commands` - Commands list  
`console` - Specifies whether run a command by console or player. (true/false)   

#### sound
`sound` - Sound name. Here you can find all of them: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html   

#### money
`money` - Amount of money given to the player

#### firework
`launch` - If true firework will lauch with the power `power`. If false it will explode instantly.  
`power` - Distance. 0 by default (~4-5 blocks)   
`effects` - Effects list. If param isn't set the effect will be random. 

It's a way easier to configure firework effects in the config:

```json
firework{
    launch=false
    power=0
    effects:[
        {
            type=BALL // Available types: BALL, BALL_LARGE, STAR, BURST, CREEPER
            trail=false
            colors:[
                "FFFFFF",
                "FF0000"
            ]
            fadeColors:[
                "000000",
                "00FF00"
            ]
        }
    ]
}
```

Colors must be set in RGB. The number of effects is unlimited.

Here is an example with each type of easter eggs and actions:

```json
name="example"
hide="false"

"0"{
    location="world, 0.0, 0.0, 0.0, 0.0, 0.0"
    type="BLOCK"
    actions{
        firework{
            launch=false
            power=0
            effects:[
                {
                    type=BALL // Available types: BALL, BALL_LARGE, STAR, BURST, CREEPER
                    trail=false
                    colors:[
                        "FFFFFF",
                        "FF00FF"
                    ]
                    fadeColors:[
                        "000000",
                        "FF0000"
                    ]
                },
                {
                    ...
                }
            ]
        }
        message{
            messages:[
                "Hello world!",
                "..."
            ]
            title="Title text"
            subtitle="subtitle text"
            actionbar="Actionbar text"
            fadeIn=10
            stay=40
            fadeOut=10
        }
        command{
            commands:[
                "ban %player%",
                "time set 0"
            ]
            console=true
        }
        sound="BUKKIT_SOUND_NAME"
        money=1000
    }
}
```

## Finish

For each group you can add actions which performs when a player finds all easter eggs from that group.
You just need to add the `onFinish` to the group file and add actions that you need to perform. 

```json
name="example"
hide=false

...

onFinish{
    sound="UI_TOAST_CHALLENGE_COMPLETE"
    message{
        title="Congratulations!"
        subtitle="You found them all!"
        fadeIn=10
        fadeOut=10
        stay=50
    }
}
```

## Placeholders
Here are the list of placeholders you can use:   
`%player%` - Player name   
`%found%` - The number of easter eggs that player found in the group   
`%count%` - Total number of easter eggs in the group  
`%category%` - The name of the group in which easter egg found by the player belongs to.  

Also, PlaceholderAPI support has been added to work with third-party plugins Available placeholders:  
`%ee_found_<name>%` - Number of found easter eggs in the group `<name>`  
`%ee_count_<name>%` - Total number of easter eggs in the group `<name>`  

## NPC
To create NPC without any third-party plugins you can use `/ee npc create <name>`. 
If you want to delete NPC just type`/ee npc delete <name>` 
You can create multiple NPCs with the same name, so when you delete them they will be removing in reverse order.

You can set custom skins for NPCs. You need to upload your skin here https://mineskin.org/
and then copy the value and signature params and paste them to the npc.conf

To get the value and signature of existing minecraft account you should copy the link down below
and then change the `<uuid>` to the uuid of the minecraft account you need. 

`https://sessionserver.mojang.com/session/minecraft/profile/<uuid>?unsigned=false`

You will get a json with the value and signature of your skin.

Also, you can set a skin by the player name. Just type `/ee npc skin <NPC name> <Player name>`
When you set a skin by the player name sometimes it may take 10-15 seconds due to restrictions
on the number of requests to the Mojang servers.

Type `/ee reload` to reload all the player data, NPC and easter eggs.


## Permissions
`ee.type.<type>` - acces to someone type (example: `ee.type.default`);

`ee.user` - acces to stats;

`ee.admin` - acces to admin commands (`list, edit, create, delete, clear, tp` and etc.).